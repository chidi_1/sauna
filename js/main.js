$(document).ready(function(){

    // carousel
    $('.catalog-slider').each(function(){
        $(this).owlCarousel({
            margin: 0,
            loop: true,
            nav: true,
            items:1,
            smartSpeed: 1500,
            fluidSpeed: 1500,
            responsive:{
                1400:{
                    margin: -50
                },
                1220:{
                    margin: -30
                },
                970:{
                    margin: 0,
                    autoWidth:true
                }
            }
        });
    });

    // попап
    $('.fancy').fancybox();

    // прокрутка страницы
    $("a.js--page-slide").click(function() {
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top - 30 + "px"
        }, {
            duration: 500
        });
        return false;
    });

    // показ и скрытие бокового меню
    $(document).on('click','.js--show-menu', function(){
        $('.left-menu-opened').animate({
            'margin-left':0
        }, 200)
    });
    $(document).on('click','.js--hide-menu', function(){
        $('.left-menu-opened').animate({
            'margin-left':-310
        }, 200)
    });

    // показать мобильное меню
    $(document).on('click', '.js--show-mobile-menu', function(){
        $('.main-nav-mobile').slideToggle(200)
        return false;
    });

    // показать поиск
    $(document).on('click', '.js--search-show', function(){
        if($(this).hasClass('open')){
            $('.header--right-block .header--search').animate({
                'width': 0
            }, 200);
            $('.js--search-show').removeClass('open')
            return false;
        }
        else{
            $('.header--right-block .header--search').animate({
                'width': 350
            }, 200);
            $('.js--search-show').addClass('open')
            return false;
        }
    });
    $(document).on('click', 'body', function(){
         $('.header--right-block .header--search').animate({
                'width': 0
            }, 200);
            $('.js--search-show').removeClass('open')
    });
    $('.header--search .header--search-input').click(function(){
        return false
    });

    // триггеры слайдера
    $(document).on('click', '.js-trigger-next', function(){
        $(this).parents('.catalog-slider').find('.owl-next').trigger('click')
        return false;
    })
    $(document).on('click', '.js-trigger-prev', function(){
        $(this).parents('.catalog-slider').find('.owl-prev').trigger('click')
        return false;
    })
});